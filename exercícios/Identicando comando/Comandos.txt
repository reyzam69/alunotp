Identifique a finalidade dos seguintes comandos:

a) git init - Cria um novo repositório vazio.

b) git config --global user.name "turing" - Altera o nome associado aos commits do Git, no caso para "Turing"

c) git add EXERCICIO.txt - Adiciona um arquivo especifíco á área de Staging, no caso "EXERCICIO.txt".

d) git add . - Adiciona uma alteração no diretório ativo á area de Staging

e) git commit -m "Adicionado nova interface" - Cria um commit com uma mensagem de commit transmitida.

f) git commit - Abre um editor de texto solicitando a você uma mensagem de commit.

g) git reset --hard HEAD - Descarta as alterações na área de stage como também reverte todas as alterações no
diretório de working para o estado do commit que foi especificado no comando.

h) cd Downloads - Muda as sessões terminais atuais que trabalham no diretório para o arquivo do diretório passado.

i) pwd - Usado para mostrar o diretório de trabalho atual.

j) cd ..

k) ls - Lista o conteúdo do diretório atual e, por padrão, não mostra os arquivos ocultos.

l) git pull - Envia o conteúdo do repositório local para o remoto.

m) git push - Envia o conteúdo do repositório remoto para o local.

n) git clone https://gitlab.com/rVenson/linguagemdeprogramacao - Cria uma cópia de um repositório especifico em
um repositório local.

o) git diff - realiza uma função de comparação nas fontes de dados Git. 
Essas fontes de dados podem ser commits, ramificações, arquivos e outros.

p) git show

Descreva a função dos seguintes componentes do Git:

1) Stage Area, Commit - A Stage Area é um pré-diretório antes da Branch local. Já o Commit envia
os arquivos da Stage Area para a Branch local.

2) Local Repository - É um diretório onde os arquivos do seu projeto ficam armazenados dentro do computador.

3) Remote Repository -É um repositório comum em que todos os membros da equipe usam para trocar suas alterações.
Esse repositório remoto é armazenado em um serviço de hospedagem de código como o GitHub / GitLab.
